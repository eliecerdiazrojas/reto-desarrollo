//////////////////////
//CREATE SERVER
//////////////////////
require('dotenv').config();
const koa = require('koa');
const static = require('koa-static');
const views = require('koa-views');
const path = require('path');

//////////////////////
//Connecting to DB
//////////////////////
const mongoose = require('mongoose');
const db = mongoose.connection;
const host = process.env.host;
const dbupdate = {
    useNewUrlParser: true,
    useUnifiedTopology: true
};

mongoose.connect(host, dbupdate);

db.on('error', (err) => console.log('Error, DB no conectada'));
db.on('connected', () => console.log('conectado a mongo'));
db.on('disconected', () => console.log('desconectado de mongo'));
db.on('open', () => console.log('conexion establecida'));


////////////////////////
//render
//////////////////////
const render = views(__dirname + '/views', {
    map: {
      html: 'underscore'
    }
});

const server = new koa();

server.use(static('./public'));
server.use(render);

const routes = require('./rutas/peliculas');

server.use(routes.route);

server.listen(8080, 'localhost', () => console.log('listening on port 8080'));
