const Router = require('koa-router');
const Peliculas = require('../model/pelicula');

const route = new Router();

route.get('/', async (ctx, next) => {
    try {
         return await Peliculas.find({}, function (error, results) {
            console.log(results);
             ctx.render('index', { posts: results });
         }).clone();
     } catch (error) {
         console.log(error);
     }
 });

 route.get('/peliculas', async (ctx, next) => {
    try {
             ctx.render('index', { posts: {title: "tlotr", year :'2020'} });
     } catch (error) {
         console.log(error);
     }
 });

 module.exports = route;