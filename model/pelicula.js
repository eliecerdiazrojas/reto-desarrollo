const mongoose = require('mongoose');
const schema = mongoose.Schema;

const peliculaShema = new mongoose.Schema({
    title: String,
    year: String,
    released: String,
    genre: String,
    director: String,
    actors: String,
    plot: String,
    ratings: String,
});

const peliculas = mongoose.model('Peliculas', peliculaShema);

module.exports = peliculas;